FROM node:12.13.0-alpine AS build-stage

COPY src /tmp/
RUN cd /tmp \
  && npm install --global @vue/cli@latest \
  && npm install && npm run build \
  && cp -R /tmp/dist /app
 
###############################################################################
FROM nginx:1.17.5-alpine

COPY --from=build-stage /app/ /usr/share/nginx/html/base/
COPY src/sites/ /usr/share/nginx/html/

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
EXPOSE 80/tcp 443/tcp
WORKDIR /usr/share/nginx/html

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
