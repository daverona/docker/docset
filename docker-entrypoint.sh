#!/bin/sh
set -e

## Configure nginx

# Create an nginx configuration for the base site
sed -e "2s|80;$|80 default_server;|" \
  -e "9s|html;$|html/base;|" \
  -e '$ d' \
  /etc/nginx/conf.d/default.conf > /etc/nginx/conf.d/base.conf

# Remove the last curly brace
sed -i -e '$ d' /etc/nginx/conf.d/base.conf

for d in /usr/share/nginx/html/*; do
  [ ! -d ${d} ] && continue;
  site="${d%/}";
  site="${site##*/}";
  [ ${site} == 'base' ] && continue;

  server="127.0.0.1:80";
  if [ -f "${d}/proxied-port" ]; then
    port=`cat ${d}/proxied-port | xargs`
    server="${site}:${port}";
  else
    # Create an nginx configuration file for the site
    sed -e "3s|localhost;$|${site};|" \
      -e "9s|html;$|html/${site};|" \
      /etc/nginx/conf.d/default.conf > "/etc/nginx/conf.d/${site}.conf";
  fi;

  # Set up an upstream for the site
  echo -e "upstream ${site} {
    server ${server};
}" | cat - /etc/nginx/conf.d/base.conf >> /tmp/base.conf.tmp;
  mv /tmp/base.conf.tmp /etc/nginx/conf.d/base.conf;
  echo -e "    location /${site}/ {
        rewrite ^/${site}(/.*)$ /\$1 break;
        proxy_pass http://${site};
        proxy_set_header Host ${site};
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Proxy-From localhost;
        proxy_redirect off;
    }" >> /etc/nginx/conf.d/base.conf;
done;

# Putthe last curly brace removed back
echo "}" >> /etc/nginx/conf.d/base.conf

rm -rf /etc/nginx/conf.d/default.conf

exec "$@"
