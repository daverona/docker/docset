# daverona/docset

[![pipeline status](https://gitlab.com/toscana/docker/docset/badges/master/pipeline.svg)](https://gitlab.com/toscana/docker/docset/commits/master)

* GitLab source directory: [https://gitlab.com/toscana/docker/docset](https://gitlab.com/toscana/docker/docset)
* Docker Hub directory: [https://hub.docker.com/r/daverona/docset](https://hub.docker.com/r/daverona/docset)

## Installation

Install [Docker](https://hub.docker.com/search/?type=edition&offering=community)
if you don't have one. Then pull the image from Docker Hub repository:

```bash
docker image pull daverona/docset
```

or build the image:

```bash
docker image build \
  --tag daverona/docset
  .
```

## Quick Start

Run the container:

```bash
docker contaienr run --rm \
  --detach \
  --publish 80:80 \
  daverona/docset
```

Visit [http://localhost](http://localhost) and verify that DocSet works.

## Customization

In this section we explain how to customize DocSet server.

Create `sites.json`:

```json
{
  "title": "Welcome to My documentation",
  "name": "My documentation",
  "items": [
    {"name": "User guide", "icon": "mdi-book", "link": "site1/", "visible": true},
    {"name": "Developer guide", "icon": "mdi-book", "link": "site2/", "visible": false},
    {"name": "REST APIs", "icon": "mdi-code-tags", "link": "redoc1/", "visible": false},
    {"name": "Another REST APIs", "icon": "mdi-code-tags", "link": "redoc2/", "visible": false},
    {"name": "Hankyoreh", "icon": "mdi-newspaper", "link": "//www.hani.co.kr", "visible": false},
    {"name": "Google", "image": "logo.png", "link": "//www.google.com", "external": true},
  ],
  "holder": "My home"
}
```

Be careful with dangling commas.


```bash
docker container run --rm \
  --detach \
  --volume $PWD/default/sites.json:/usr/share/nginx/html/default/sites.json:ro \
  --volume $PWD/default/favicon.ico:/usr/share/nginx/html/default/favicon.ico:ro \
  --volume $PWD/default/logo.png:/usr/share/nginx/html/default/logo.png:ro \
  --volume $PWD/site1:/usr/share/nginx/site1 \
  --volume $PWD/site2:/usr/share/nginx/site2 \
  --volume $PWD/redoc1:/usr/share/nginx/redoc1 \
  --volume $PWD/redoc2:/usr/share/nginx/redoc2 \
  --publish 80:80 \
  daverona/docset
```
